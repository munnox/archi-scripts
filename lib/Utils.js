﻿/*
 * Open A CSV file and match them to the current model
 * Authored by Robert Munnoch
 * Inspired by:
 * https://www.papaparse.com/
 * https://github.com/rchevallier/jarchi-lib/blob/main/scripts/Import%20from%20CSV.ajson
 * https://github.com/rchevallier/jarchi-lib/blob/main/scripts/Export%20View%20to%20CSV.ajs
 * Apache or GPLv3
 * Basically free to use but do not blame me if it stops working and please let me know if it could be improved.
 */

var __DIR__ = __DIR__ || __dirname + "/";
var __SCRIPTS_DIR__ = __SCRIPTS_DIR__ || "../";

Object.defineProperty(exports, "__esModule", { value: true });
const Papa = require(__DIR__+'../lib/papaparse.min.js');
// load('https://unpkg.com/papaparse@latest/papaparse.min.js');

const { CellInfo, GridIndex, Grid, Bounds, Box, Point, Padding } = require(__DIR__ + "../dist/view_objects.js");

exports.CellInfo = CellInfo;
exports.GridIndex = GridIndex;
exports.Grid = Grid;
exports.Bounds = Bounds;
exports.Box = Box;
exports.Point = Point;
exports.Padding = Padding;

class RunCommandError extends Error {
  process = null;

  constructor(message, process, options) {
    // Need to pass `options` as the second parameter to install the "cause" property.
    super(message, options);
    this.process = process;
  }
}

class WebRequestError extends Error {
  request = null;

  constructor(message, request, options) {
    // Need to pass `options` as the second parameter to install the "cause" property.
    super(message, options);
    this.request = request;
  }
}

class RAMArchi {

// View Elements helpers
static CreateNote(view, name, bounds) {
    let note = view.createObject("note", bounds.x, bounds.y, bounds.width, bounds.height);
    note.setText(name);
    // note.textPosition = TEXT_POSITION.CENTER;
    // note.textAlignment = TEXT_ALIGNMENT.CENTER;

    // note.borderType = BORDER.RECTANGLE;
    // note.borderType = BORDER.TABBED;
    return note;
}

static CreateGroup(view, name, bounds) {
    let grp = view.createObject("group", bounds.x, bounds.y, bounds.width, bounds.height);
    grp.setName(name);
    // grp.borderType = BORDER.RECTANGLE;
    // grp.borderType = BORDER.TABBED;
    return grp;
}

static CreateViewElement(view, concept, bounds) {
    let ele = view.add(concept, bounds.x, bounds.y, bounds.width, bounds.height);
    // ele.borderType = BORDER.RECTANGLE;
    // ele.borderType = BORDER.TABBED;
    // ele.setLabelExpresstion("${name}");
    // ele.labelExpression = "${name}";
    return ele;
}

static DrawGroupWithContents(
    view,
    nested_object,
    bounds,
    elements,
    rows, columns,
    draw,
    padding=null, unit=12
) {
    let nested_object_is_string = (typeof nested_object) === "string";
    let grp = null;
    let name_view_type = null;
    if (nested_object_is_string) {
        name_view_type = "string";
        grp = nested_object;
    }
    // Create the group or element if the view was passed in
    if (view) {
        if (nested_object_is_string) {
            grp = RAMArchi.CreateGroup(view, nested_object, bounds);
            name_view_type = "string";
        } else {
            try {
                grp = RAMArchi.CreateViewElement(view, nested_object, bounds);
                name_view_type = "concept";
            } catch (error) {
                name_view_type = "noconcept";
            }
        }
    } else {
        // NOTE: Not hugly happy with this but it gets the idea of of the original design a
        // non emptry object should get "concept" type.
        // Might be removed in future...
        if (
            (!nested_object_is_string) // If not a string
            && ( // And truthy and an object with keys <- No good way to test for a Archi element
                nested_object
                && typeof nested_object === "object"
                && Object.keys(nested_object).length > 0
            )
        ) {
            name_view_type = "concept";
            grp = nested_object;
        }
    }
    // console.log(`nested_object: ${nested_object} - type: ${name_view_type}`);

    // console.log(`name type: ${typeof name} ${name_view_type} ${name === null} name='${name}' grp='${grp}'`);
    let group_nesting_flag = false;
    // group_nesting_flag = true;

    // If a concept OR String and not empty
    if ((name_view_type === "concept") || ((name_view_type === "string") && (nested_object !== ""))) {
        // Then the contents needs to the nested within
        group_nesting_flag = true; 
    }

    // If padding was not given then calculate it from a single value taking into account the nesting of the content.
    if (!padding) {
        padding = Padding.FromSingle(unit, group_nesting_flag);
        // console.log(`${group_nesting_flag} - ${padding}`)
    }

    // console.log(`Draw Group: ${grid_contents}, padding: ${padding}, group_nesting_flag: ${group_nesting_flag}`);
    let content_bounds = null;
    if (grp) {
        content_bounds = Bounds.CloneAndResetOrigin(bounds);
    } else {
        content_bounds = Bounds.Clone(bounds);
    }
    // console.log(`Calculating the inner bounds: ${bounds} -[${grp}]-> ${content_bounds}`)

    // Build a grid to layout the contents
    let grid_contents = Grid.calcFromCount(elements.length, rows, columns);
    grid_contents.bounds = content_bounds;
    grid_contents.padding = padding;
    grid_contents.process(
        (cell) => {
            let data_obj = elements[cell.indexes.index];
            let newcell = new CellInfo(
                cell.indexes,
                cell.bounds,
                padding,
                data_obj,
                grp,
                {
                    element: data_obj,
                    parent_element: grp,
                    nested_flag: group_nesting_flag,
                    view,
                    name: nested_object,
                    concept: nested_object,
                    name_view_type
                }
            );
            // console.log(`Cell process: ${cell}`);
            draw(
                newcell
                // grp,
                // data_obj,
                // cell_bounds
            );
        },
        // padding,
        // unit,
        // group_nesting_flag
    );
    return grp;
}

// View interface helpers

static GetView(name) {
    // Get a collection named views and return the collection
    let view = $("archimate-diagram-model." + name);
    return view;
}

static EnsureCleanView(name) {
    // to limit new view need to find the view and if found then remove it
    let view = RAMArchi.GetView(name).first();
    // If the view exist either delete it or clear it
    if (view != null) {
        // view.delete();
        $(view).children().each((element) => {
            element.delete();

        })
    }
    else {
        // Since elements are being removed the view only needs
        // to be created when its not found.
        view = model.createArchimateView(name);
    }
    return view;
}

// Render to a HTML page

static BrowserView(html_text) {
    // Refenence -> https://gist.githubusercontent.com/ThomasRohde/8daf0a5c5814b597953983d0a549b04c/raw/c4ce6379a0c2643ea5ce64a4e01b85e7f63b6fd8/Browser%2520integration.ajs
    // Orignal Example Author: Thomas Klok Rohde
    // Tweaking Author: Robert Munnoch
    // Simplified it down to the bare elements to start a MVP for building advanced views within the browser.
    // minimise by Robert Munnoch and a d3 example added to try further fancyness...
    // This does seem to want to use bootstrap or D3 but the cross call does work as illustrated
    // Moveing it in to a reuseable function in the utilities.

    // This seem to be sorted at least for Edge the internal browser cannot, will not, not sure why, render
    // Also Tried it on linux works fine about from no slash on the tmp folder... but firefox works well.
    const BrowserEditorInput = Java.type('com.archimatetool.editor.browser.BrowserEditorInput');
    const IBrowserEditor = Java.type('com.archimatetool.editor.browser.IBrowserEditor');
    const EditorManager = Java.type('com.archimatetool.editor.ui.services.EditorManager');
    const CustomFunction = Java.extend(Java.type('org.eclipse.swt.browser.BrowserFunction'));

    // Write the HTML to a temporary file, so we are allowed to execute a local script
    let System = Java.type('java.lang.System');
    let tmpfile = System.getProperty("java.io.tmpdir") + "/testbrowser.html";
    $.fs.writeFile(tmpfile, html_text);
    // console.log(tmpfile);

    let input = new BrowserEditorInput("file:///" + tmpfile, "Test browser");
    let browser = EditorManager.openEditor(input, IBrowserEditor.ID).getBrowser();
    let display = shell.getDisplay();

    return [
        CustomFunction,
        browser,
    ]

}

static MoveFolder(foldername, view) {
    let folder = $(foldername).first();
    folder.add(view);
}

// Archi graph traversal

static ModelQuery(base_selection, debug=false) {
    debug? console.log(base_selection, typeof base_selection) : null;
    const [main, query] = base_selection.split("?");
    debug? console.log(`Main: ${main} , query: ${query}`) : null;

    let collection = $(`${main}`);

    if (query && query != "") {
        const query_obj = {};
        const parts = query.split("&");
        parts.forEach((part) => {
            const [key, value] = part.split("=");
            const clean_value = value == '' ? null : value;
            query_obj[key] = clean_value;
        });
        debug? console.log(`query: ${JSON.stringify(query_obj)}`) : null;

        collection = collection.filter((ele) => {
            const keys = Object.keys(query_obj);
            return keys.every((key) => {
                const result = ele[key] === query_obj[key] || ele.prop(key) === query_obj[key];
                debug? console.log(`Key ${key} - ${query_obj[key]} ${result}`) : null;
                return result;
            });
        });

    }
    return collection;
}

static GetParents(ele, filter_relationship) {
    if (filter_relationship === null || filter_relationship === undefined) {
        return $(ele).inRels().sourceEnds();
    } else {
        return $(ele).inRels(filter_relationship).sourceEnds();
    }
}

/**
 * Get a list of object representing the graph paths from the current node to sibling via which parent was found.
 * @param {*} ele 
 * @param {*} filter_relationship 
 * @returns 
 */
static GetSiblingPaths(ele, filter_relationship) {
    
    let siblings = [];
    let parent_nodes = null;
    
    if (filter_relationship === null || filter_relationship === undefined) {
        parent_nodes = $(ele).inRels().sourceEnds();
    } else {
        parent_nodes = $(ele).inRels(filter_relationship).sourceEnds();
    }
    parent_nodes.each(function(p) {
        // let 
        let r = $(p).outRels(); //filter_relationship); //.targetEnds();
        r.each(r => {
            let s = $(r).targetEnds();
            s.each(sibling => {
                // console.log(`* P(${p})-[${r}]->S(${sibling})`);
                siblings.push({
                    parent: {
                        concept: p
                    },
                    // This seems to get a weired object type with the following methods
                    // merge,getJunctionType,setJunctionType,setType,delete,setSpecialization,getConcept,getSpecialization,getName,equals,toString,hashCode,compareTo,setName,getId,getType,prop,getModel,getDocumentation,setDocumentation,removeProp,getLabelExpression,setLabelExpression,getLabelValue
                    concept: sibling.getConcept(),
                    link: {
                        concept: r
                    }
                });
            });
        })
        // console.log(`Source: ${$(r).sourceEnds()}`);
    })
    // console.log(`parent: ${parent_nodes}, siblings: ${siblings.length}`);
    return siblings;
    // if (filter_relationship == null) {
    //     return $(ele).inRels().sourceEnds().outRels().targetEnds();
    // } else {
    //     return $(ele).inRels(filter_relationship).sourceEnds().outRels().targetEnds();
    // }
}

static GetSiblings(ele, filter_relationship) {
    if (filter_relationship === null || filter_relationship === undefined) {
        return $(ele).inRels().sourceEnds().outRels().targetEnds();
    } else {
        return $(ele).inRels(filter_relationship).sourceEnds().outRels().targetEnds();
    }
}

static GetChildren(ele, filter_relationship) {
    if (filter_relationship === null || filter_relationship === undefined) {
        return $(ele).outRels().targetEnds();
    } else {
        return $(ele).outRels(filter_relationship).targetEnds();
    }
}

// Element properties

static MergeProperties(element, new_properties, edit_concept=false) {
    for (let prop_name of Object.keys(new_properties)) {
        let value_new = new_properties[prop_name]
        if (typeof value_new !== 'array') {
            value_new = [value_new]
        }
        value_new.forEach(val => {
            // console.log(`Prop type: ${typeof val}, element name: ${element.name} key: ${prop_name}=${val}`);
            if (typeof val == 'object') {
                edit_concept ? element.prop(prop_name, JSON.stringify(val), true) : null;
            } else {
                edit_concept ? element.prop(prop_name, JSON.stringify(val), true) : null;
            }
        })
    }
}

static AlignProperties(element, classtype, edit_concept=false, reorder=false) {

    // console.log(element.name, (new type).constructor.name);
    let current_properties = {};
    let prop_list = element.prop()
    prop_list.forEach((prop) => {
        current_properties[prop] = element.prop(prop,true);
        edit_concept && reorder ? element.removeProp(prop): null;
    })
    // console.log(`${element.name}, ${(new classtype).constructor.name}, ${current_properties}`);
    
    let key_on_object = RAMUtils.ArrayDiff(
        Object.keys(current_properties),
        classtype.element_properties.map((prop) => {return prop[0];})
    );

    // console.log(`Diff '${key_on_object}'`);
    
    // Put the properties pull back
    classtype.element_properties.forEach((prop) => {
        // console.log(`Prop: ${prop}`)
        let [name, proptype, def] = prop
        let value_old = current_properties[name]
        // let random = default;
        if (value_old) {
            // console.log(`Val old: ${value_old}`);
            if (reorder) {
                // if reordering then the properties are removed and need to be added back
                value_old.forEach((val) => {
                    edit_concept ? element.prop(name, val, true) : null;
                });
            }
            
        } else {
            // console.log(`Undefined old value: ${value_old}`);
            edit_concept ? element.prop(name, def, true) : null;
        }
        // value_old.
    })

    // Add the rest of the properties back on at the end
    key_on_object.forEach((prop_name) => {
        let value_old = current_properties[prop_name]
        if (value_old) {
            // console.log(`Val old: ${value_old}`);
            value_old.forEach((val) => {
                edit_concept ? element.prop(prop_name, val, true) : null;
            })
        }       
    })
}

static GetPropMap(concept) {
    if (concept === null) return null;
    // console.log(`concept: ${concept}`);
    let props = {}; //new Map();
    let prop_list = concept.prop();
    for (let prop_name of prop_list) {
        props[prop_name] = concept.prop(prop_name, true);
    }
    return props;
}

/**
 * Match a list of object with id or name to Jarchi model element by searching by id or name.
 * @param {*} objects List of object with an archi id or name in properties defined by `id_key` or `id_name`
 * @param {*} id_key Name of property containing the object id
 * @param {*} name_key Name of property container the object name 
 * @param {*} debug Flag for console debugging
 * @returns Returns a list of  objects and matches along with the type of the match id or name.
 */
static ModelMatchingByKey(objects, id_key='id', name_key="name", debug=false) {
    matched_objects = [];

    for (var ind=0;ind<objects.length;ind++) {
        var obj = objects[ind];
        debug? console.log(`Object: ${obj}`) : null;

        var ele = $(`#${obj[id_key]}`).first();
        var type = id_key;
        if (!ele) {
            ele = $(`.${obj[name_key]}`).first();
            type = name_key;
            if (!ele) {
                ele = null;
                type = null;
            }
        }

        match = {
            object: obj,
            element: ele,
            type: type
        }
        matched_objects.push(match);
    }
    return matched_objects;
}

// Element views

/**
 * Helper to identify  a model node element.
 * @param {*} concept jArchi Model Concept
 * @returns 
 */
static isNode(concept) {
    return concept.source === undefined  && concept.target === undefined;
}

/**
 * Helper to identify  a model node element.
 * @param {*} concept jArchi Model Concept
 * @returns 
 */
static isRelationship(concept) {
    return !RAMArchi.isNode(concept);
}

/**
 * Helper function to take a model concept and find all the fill colours across all the view instances.
 * 
 * This also allow the default count to be removed and a debug flag to be set
 * @param {*} concept jArchi Model Concept
 * @param {boolean} debug Boolean Flag for debugging
 * @returns {Object} An JS object with the colours as keys an counts of frequency
 */
static GetElementFillColors(concept, debug=false) {
    // Shortcut to null if the concept given is null to not let nulls hide
    if (concept === null) {
        return null;
    }
    let color_count = { };
    // Run through all the view elements to find the colours and count them
    $(concept).objectRefs().each(ref => {
        const fillkey = ref.fillColor || "default";
        // debug? console.log(`Object ref ${ref} Colour Ref ${ref.fillColor}`) : null;
        let color_number = color_count[fillkey];
        if (color_number === undefined) {
            color_number = 1;
        } else {
            color_number += 1;
        }
        color_count[fillkey] = color_number;
    })
    // If debugging show the color counts
    debug? console.log(`Concept '${concept}' color counts: ${JSON.stringify(color_count)}`) : null;
    
    // const color_keys = Object.keys(color_set);
    return color_count;

}

}

exports.RAMArchi = RAMArchi;

class RAMArchiFile {

static ReadFile(filepath) {
    // https://github.com/rchevallier/jarchi-lib/blob/main/scripts/Import%20from%20CSV.ajson
    const FileReader = Java.type("java.io.FileReader");
    const StandardCharsets = Java.type("java.nio.charset.StandardCharsets");
    const rawFile = new FileReader(filepath, StandardCharsets.UTF_8);

    let encodedFile ="";
    
    let data = rawFile.read();
    while(data != -1) {
        encodedFile+=String.fromCharCode(data);
        data = rawFile.read();
    }
    rawFile.close();

    return encodedFile;
}

static ReadCSV(filepath, delimiter = ",") {

    const encodedFile = RAMArchiFile.ReadFile(filepath);

    const parsed_csv = Papa.parse(encodedFile, {delimiter: delimiter});

    const headers = parsed_csv.data[0];

    var data = [];
    for (let i=1; i<parsed_csv.data.length; i++) {
        let keyed_obj = {};
        for (let j=0; j<headers.length; j++) {
            keyed_obj[headers[j]]=parsed_csv.data[i][j];
        }
        data.push(keyed_obj);
    }

    const result = {
        "headers": parsed_csv.data[0],
        "data": data
    }
    if (parsed_csv.errors.length > 0) {
        console.log(JSON.stringify(parsed_csv.errors.length));
        console.log(JSON.stringify(parsed_csv.errors));
        return null
    }
    return result;
}

static WriteFile(filepath, data) {
    // https://github.com/archimatetool/archi-scripting-plugin/wiki/The-Console%2C-Dialogs-and-Other-Utilities#fswritefile
    // https://github.com/rchevallier/jarchi-lib/blob/main/scripts/Export%20View%20to%20CSV.ajs


    if(filepath != null) {
        $.fs.writeFile(filepath, data, "UTF-8");
    }
    else {
        console.log("> Export cancelled");
    }
}

static WriteFileBinary(filepath, data) {
    // https://github.com/archimatetool/archi-scripting-plugin/wiki/The-Console%2C-Dialogs-and-Other-Utilities#fswritefile
    // https://github.com/rchevallier/jarchi-lib/blob/main/scripts/Export%20View%20to%20CSV.ajs

    if(filepath != null) {
        $.fs.writeFile(filepath, data, "BASE64");
    }
    else {
        console.log("> Export cancelled");
    }
}

static WriteCSV(filepath, data, headers=null, delimiter=",") {
    // https://github.com/rchevallier/jarchi-lib/blob/main/scripts/Export%20View%20to%20CSV.ajs

    // if not given then use the keys of the first object.
    if (!headers) {
        headers = Object.keys(data[0]);
    }

    var prepared_file = Papa.unparse({fields:headers, data: data}, {delimiter: delimiter, quotes: true});

    RAMArchiFile.WriteFile(filepath, prepared_file);
}

}

exports.RAMArchiFile = RAMArchiFile;

class RAMArchiExternal {

static getArchiEnv(name, default_value) {
    // https://docs.oracle.com/javase/8/docs/api/java/lang/System.html
    const System = Java.type("java.lang.System");
    let envvar = System.getenv(name) || default_value || null;
    return envvar;
}

// Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --script.runScript 'ram/Run API.ajs' --myArg "HelloWorld" 
static getArchiArgs() {
    return $.process.argv;
}

// Web API's

/**
 * Encode a string to a Base64 string within the GraalVM framework
 * @param {string} s A String to encode into Base64 
 * @returns 
 */
static encodeBase64(s) {
    var Base64 = Java.type('java.util.Base64');
    // var String = Java.type('java.String');
    // console.log(JSON.stringify(Object.keys(String)));

    let enc = Base64.getEncoder().encode(s.getBytes());
    let out = '';
    enc.forEach(b => {
        out += String.fromCharCode(b);
    })
    return out;
    // return Base64.encode(s.getBytes()).toString();
}

/**
 * Decode a base64 to a string within GraalVM framework
 * @param {string} s A base64 encoded string 
 * @returns 
 */
static decodeBase64(s) {
    var Base64 = Java.type('java.util.Base64');
    // var String = Java.type('java.String');
    // console.log(JSON.stringify(Object.keys(String)));

    let dec = Base64.getDecoder().decode(s.getBytes());
    let out = '';
    dec.forEach(b => {
        out += String.fromCharCode(b);
    })
    return out;
    // return Base64.decode(s.getBytes()).toString();
}

/**
 * Make a api web request from the given url and quewry and a map of headers with an optional methods
 * @param {string} url URL to fetch
 * @param {Object} body_object A json bosy for the request
 * @param {Object} headers A map for the headers of the request
 * @param {string} method A Optional args for the request methods defaults to GET
 * @returns 
 */
static WebRequest(url, body_object, headers, method="GET", verify=true) {
    // Abiltity to send web requests from JS in GraalVM
    // Derived from https://gist.github.com/cybye/9ed9bd1dc2c53906d7de4fcccfdd5b99 2024_01_13 see external_examples/gpt.ajs
    // improved with https://stackoverflow.com/questions/1359689/how-to-send-http-request-in-java
    // Docs with https://docs.oracle.com/javase/tutorial/networking/urls/readingWriting.html
    // Cannot get this to work from a untrusted certificate site... so would need external or lets-encrypt to get a globally trusted certificate.

    let java_imports = new JavaImporter(
        java.net, java.util, java.lang,
        java.io, java.nio.charset, javax.net.ssl
    );

    let request = {
        url: url,
        headers,
        method,
        verify,
        body: body_object,
        // method: urlconnection.getRequestMethod(),
        // code: urlconnection.responseCode,
        // data: null,
        // type: "test/plain",
    };
    let responce = {
        url: url,
        request,
        // method,
        // method: urlconnection.getRequestMethod(),
        // code: urlconnection.responseCode,
        data: null,
        type: "test/plain",
    };
    let result = "";

    // console.log(java_imports);

    let urlObj =  new java_imports.URL(url)
    let urlconnection   = urlObj.openConnection()

    /**
     * Standard Java from Stackoverflow (I think - lost ref) for SSL trust for translation into javascript below
     *  TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
     *     public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null; }
     *     public void checkClientTrusted(X509Certificate[] certs, String authType) {}
     *     public void checkServerTrusted(X509Certificate[] certs, String authType) {}
     * } };
     * SSLContext sslContext = SSLContext.getInstance("SSL");
     * sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
     * HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
     * 
     * Does seem to to be picked up although it does seem to "run"
     * Next attempt might be this one or move it into a bundled JAR file
     * https://stackoverflow.com/questions/2012497/accepting-https-connections-with-self-signed-certificates
     * 
    */

    if (!verify) {
        // To javascript GraalVM to hopfully trust all certificates
        const X509TrustManager = Java.type("javax.net.ssl.X509TrustManager");
        const SecureRandom = Java.type("java.security.SecureRandom");
        const random = new SecureRandom();

        let Ext = Java.extend(X509TrustManager);
        let trustAllCerts = [
            // new X509TrustManager()
            new Ext({
                // Return certificate authority paths
                getAcceptedIssuers() {
                    // console.log("Accept");
                    return null;
                },
                // Called to check client certs
                checkClientTrusted(certs, authType) {},
                // Called to check server certs
                checkServerTrusted(certs, authType) {
                    // console.log(`Server certs: ${certs}`)
                },
            })
        ];

        // Apply the extended trustmanager to the ssl context
        // let sslContext = java_imports.SSLContext.getDefault(); // Doesn't work Failed to make a SSL instance
        // let sslContext = java_imports.SSLContext.getInstance("TLS"); // Can work
        let sslContext = java_imports.SSLContext.getInstance("SSL"); // Can work using this as this was was in the example
        // console.log(sslContext);
        sslContext.init(null, trustAllCerts, random );
        
        urlconnection.setSSLSocketFactory(sslContext.getSocketFactory());
    }

        urlconnection.setRequestMethod(method);
        // console.log(`URL Objects: ${urlObj}, ${Object.keys(urlconnection)} ${urlconnection.getRequestMethod()}`);
        // console.log(urlObj);
        // console.log(urlconnection);

        const header_keys = headers? Object.keys(headers) : [];
        for (let h_index=0;h_index<header_keys.length;h_index++) {
            let key = header_keys[h_index];
            let value = headers[key];
            urlconnection.setRequestProperty(key,value);
        }
        // urlconnection.setRequestProperty ("Content-Type", "application/json; charset=UTF-8")
        // urlconnection.setRequestProperty ("api-key", api_key)
        // urlconnection.setRequestProperty ("user-agent", "curl/7.81.0")
        // urlconnection.setRequestProperty ("accept", "*/*")

        // Start the connection I do not need both keeping for reference
        // urlconnection.connect();
        try {
            urlconnection.setDoOutput(true);
        } catch (error) {
            
            console.error(error);
            throw new WebRequestError(`${error}`, responce, {cause: error});
        }

        // console.log(`Body: ${typeof body_object} ${Object.keys(body_object)}`);
        // console.log(`URL Method: ${urlconnection.getRequestMethod()}`);

        // If there is a body to send open a outputstream to send it
        // NOTE: This will force a change of HTTP Method to POST
        if(body_object && typeof body_object === "string") {
            let outputStream = urlconnection.getOutputStream();
            // console.log(`URL Method: ${urlconnection.getRequestMethod()}`);
            let writer = new java_imports.OutputStreamWriter(outputStream);
            // let json = ""+ JSON.stringify(body_object);
            let json = "" + body_object;
            // console.log(`Body: ${typeof body_object} ${body_object}`);
            writer.write(json);
            writer.flush();
            writer.close();
        }

        let line = null;
        let result_data = "";
        let result_type = "text/plain";

        try {
            let inputStream = urlconnection.getInputStream();
            let inputReader = new java_imports.InputStreamReader(inputStream, java_imports.StandardCharsets.UTF_8);
            let reader = new java_imports.BufferedReader(inputReader);

            while ((line = reader.readLine()) != null) {
                result_data += line + "\n";
            }
            reader.close();
        }
        catch (error) {
            result_data = `${error}`;
            // console.log(`${error} ${Object.keys(error)}`);
        }


        // Try to convert the result to json fail quiet if that is not possible and just return the string
        try {
            result_data = JSON.parse(result_data);
            result_type = "application/json";
        } catch (error) {
            // throw error;
        }

        responce.method = urlconnection.getRequestMethod();
        responce.code = urlconnection.responseCode;
        responce.data = result_data;
        responce.type = result_type;
        
        return responce;
}

// Wrapping JArchi child process exec function doesn't seem to return the result
static RunArchiChildProcess(command) {
    return $.child_process.exec(...command);
}

// So since we can use Java classes using https://www.baeldung.com/run-shell-command-in-java

static RunCommandSync(command_args) {
    const ProcessBuilder = Java.type('java.lang.ProcessBuilder');

    if (!Array.isArray(command_args)) {
        throw new Error(`Command_args must be a List of strings. Given, type '${typeof command_args}' with value='${command_args}'`);
    }

    const process = new ProcessBuilder(command_args);
    
    let running = null;

    try {
        running = process.start();
    } catch (error) {
        let new_error = new RunCommandError(`Command Failed. Error: '${error}', Process: '${process}'`, process, { cause: error});
        console.error(new_error);
        throw new_error;
    }
    const inputStream = running.getInputStream();
    const scanner = new java.util.Scanner(inputStream, 'UTF-8').useDelimiter('\\A');
    const output = scanner.hasNext() ? scanner.next() : '';

    // console.log(`Output was:\n${output}`);
    return output;
}

// NOTE: So far not needed to run so commented out
// static RunCommandAsync(command) {
//     const Runtime = Java.type('java.lang.Runtime');

//     // const command = 'ls';
//     const process = Runtime.getRuntime().exec(command.split(' '));
//     const inputStream = process.getInputStream();
//     const scanner = new java.util.Scanner(inputStream, 'UTF-8').useDelimiter('\\A');

//     scanner.hasNext() && console.log(`Output was:\n${scanner.next()}`);
//     return output;
// }

}

exports.RAMArchiExternal = RAMArchiExternal;

const RAMUtils = (function() {

const utils = {};
utils.UNIT = 12;



utils.GridCalc = function(count, rows=null, columns=null) {
    console.error("Deprecated Use Grid.calcFromCount")
    return Grid.calcFromCount(count, rows, columns);

    // if ((rows == null) && (columns == null)) {
    //     // if no grid is defined then fix columns to 1
    //     columns = 1;
    // }
    
    // if ((rows === null) && (Number.isSafeInteger(columns))) {
    //     // if rows are null aqnd col is a number then calc rows
    //     // console.log(`Grid calc init total=${count} row=${rows} cols=${columns}`);
    //     rows = Math.ceil(count / columns);
    //     // console.log(`Grid calc final total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
    // } else if ((Number.isSafeInteger(rows)) && (columns === null)) {
    //     // if rows is fixed calulate cols
    //     // console.log(`Grid calc init total=${count} row=${rows} cols=${columns}`);
    //     columns = Math.ceil(count / rows);
    //     // console.log(`Grid calc final total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
    // } else {
    //     // if no grid is defined then fix columns to 1
    //     // list itterate rows
    //     // console.log(`Grid calc else total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
    //     columns = 1;
    //     rows = Math.ceil(count / columns);
    // }
    // return new Grid(rows, columns);
    // {
    //     rows,
    //     columns: columns,
    //     toString() {
    //         return `Row: ${this.rows}, Columns: ${this.columns}`;
    //     }
    // }
}

utils.ElementIn = function(element, object) {
    let prop_list = null;
    try {
        prop_list = Object.keys(object);
    } catch {}
    let inflag = false;
    if (prop_list.indexOf(element) >= 0) {
        inflag = true;
    } else {
        inflag = false;
    }
    // console.log(`ElementIn obj ${object}, ${element}, props '${prop_list}' ${inflag}`);
    return inflag;

}

utils.ArrayDiff = function(a,b) {
    // filter all unless it DOESN't Appear in the other list
    return a.filter((ele) => {
        return b.indexOf(ele) < 0;
    });
}

utils.ArraySymDiff = function(a,b) {
    // Concat a-b + b-a
    return RAMUtils.ArrayDiff(a,b).concat(RAMUtils.ArrayDiff(b,a));
}

utils.ReadFile = function(filepath) {
    console.warn("Deprecated Use RAMArchiFile.ReadFile");
    return RAMArchiFile.ReadFile(filepath);
}

utils.ReadCSV = function(filepath, delimiter = ",") {
    console.warn("Deprecated Use RAMArchiFile.ReadCSV");
    return RAMArchiFile.ReadCSV(filepath);
}

utils.WriteFile = function(filepath, data) {
    console.warn("Deprecated Use RAMArchiFile.WriteFile");
    return RAMArchiFile.WriteFile(filepath, data);
}

utils.WriteCSV = function(filepath, data, headers=null, delimiter=",") {
    console.warn("Deprecated Use RAMArchiFile.WriteCSV");
    return RAMArchiFile.WriteCSV(filepath, data, headers=headers, delimiter=delimiter);
}

// utils.NestedBoundry = function(bounds, element_callback, padding, unit=12) {
//     if (!padding) {
//         padding = new Padding(unit);
//     }

//     let nested_bounds = new Bounds(
//         new Point(
//             padding.left,
//             padding.top
//         ),
//         new Box(
//             bounds.width - (padding.left + padding.right),
//             bounds.height - (padding.top + padding.bottom)
//         )
//     );
//     returned_bounds = element_callback(bounds, nested_bounds);
    
//     return nested_bounds;
// }


// utils.GridGroup = function(bounds, grid, element_callback, padding, unit=12) {
//     if (!padding) { padding = {
//         top: unit/2,
//         bottom: unit/2,
//         left: unit/2,
//         right: unit/2,
//         framev: unit/4, // gap between columns
//         frameh: unit/4 // gap between rows
//     }; }
//     if (!grid) { grid = {rows: 1, columns: 1};}

//     // console.log(`${JSON.stringify(padding)}, ${JSON.stringify(grid)}`);

//     let nested_bounds = {
//         x: bounds.x, //padding.left,
//         y: bounds.y, //padding.top,
//         width: bounds.width, // - (padding.left + padding.right),
//         height: bounds.height, // - (padding.top + padding.bottom)
//     }

//     const row_height = (nested_bounds.height - padding.frameh*(grid.rows-1))/grid.rows;
//     const column_width = (nested_bounds.width - padding.framev*(grid.columns-1))/grid.columns;

//     // console.log(`${JSON.stringify(nested_bounds)}, ${JSON.stringify({row_height, column_width})}`);

//     for (let index_row=0;index_row<grid.rows;index_row++) {
//         for (let index_column=0;index_column<grid.columns;index_column++) {
//             // console.log(`${row}, ${column} ${JSON.stringify(grid)}`)
//             cell_bounds = new Bounds(
//                 nested_bounds.x + index_column*column_width + padding.framev*index_column,
//                 nested_bounds.y + index_row*row_height + padding.frameh*index_row,
//                 column_width,
//                 row_height,
//             );
//             let index_total = index_column + grid.columns*index_row;
//             returned_bounds = element_callback(index_total, index_row, index_column, cell_bounds);

//         }
//     }
    
//     return nested_bounds;
// }

utils.vertical_progression = function(bounds, elements, element_callback, padding=utils.unit, unit=utils.unit) {
    let default_width = 120;
    let default_height = 55;

    let width = default_width*3;
    let height = default_height*2;

    let column_offset = bounds.x;
    let row_offset = bounds.y;

    // console.log(`Elements to display ${elements.length}`);
    let index = 0;
    elements.forEach(function (ele) {
        let element_bounds = {
            x: column_offset,
            y: row_offset,
            width,
            height,
        };
        let nested_bounds = {
            x: column_offset,
            y: row_offset,
            width,
            height,
        };
        returned_bounds = element_callback(index, element_bounds, ele);

        // column_offset += returned_bounds.width + padding;
        row_offset += returned_bounds.height + unit;
        index += 1;
    });
}

utils.horizontal_progression = function(bounds, elements, element_callback, padding=12, unit=12) {
    let default_width = 120;
    let default_height = 55;

    let width = default_width*3;
    let height = default_height*2;

    let column_offset = bounds.x;
    let row_offset = bounds.y;

    // console.log(`Elements to display ${elements.length}`);
    let index = 0;
    elements.forEach(function (ele) {
        let element_bounds = {
            x: column_offset,
            y: row_offset,
            width,
            height,
        };
        let nested_bounds = {
            x: column_offset,
            y: row_offset,
            width,
            height,
        };
        returned_bounds = element_callback(index, element_bounds, ele);

        column_offset += returned_bounds.width + padding;
        // row_offset += returned_bounds.height + unit;
        index += 1;
    });
}

utils.ShowObjectKeys = function(o) {
    return Object.keys(o);
}

utils.ShowObject = function(o) {
    return JSON.stringify(o, null, 2);
}


// console.log(utils);
return utils;

})();


// class Grid {
//     constructor(rows, columns) {
//         this.rows = rows;
//         this.columns = columns;
//     }

//     toString() {
//         return `Grid(Row=${this.rows}, Columns=${this.columns})`;
//     }

//     setCellSize(width, height) {
//         this.cell = {
//             width,
//             height
//         }
//     }

//     getGridBounds(x=0,y=0,cell_bounds) {
//         this.setCellSize(cell_bounds.width, cell_bounds.height);
//         return new Bounds(
//             x,
//             y,
//             this.cell.width*this.columns,
//             this.cell.height*this.rows
//         )
//     }
// }

// class Bounds {
//     constructor(x,y,width,height) {
//         this.x = x;
//         this.y = y;
//         this.width = width;
//         this.height = height;
//     }

//     toString() {
//         return `Bounds(${JSON.stringify(this)})`;
//     }
// }

// Moved to Typescript rendering
// Depreacated ready for removal
// class BoxModel {
//     constructor(width,height) {
//         this.width = width;
//         this.height = height;
//     }

//     toString() {
//         return `BoxModel(${JSON.stringify(this)})`;
//     }
// }


exports.RAMUtils = RAMUtils;

// console.log(`Exports Utils: ${Object.keys(exports)}`)