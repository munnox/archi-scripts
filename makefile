DEV_IMAGE=archi-helper-dev:latest
DEV_RUN_NAME=archi-help-dev-run-daemon

build_dev_nix:
	sudo docker build -f containers/dev_nix/Dockerfile -t ${DEV_IMAGE} .

# --user $(shell id -u):$(shell id -g)
run_dev_nix:
	sudo docker run --rm -it \
		-v ${HOME}/.ssh:/root/.ssh:ro \
		-v ${shell pwd}/:/work/ \
		${DEV_IMAGE} \
		bash

run_dev_nix_daemon:
	sudo docker run -d --name as \
		-v ${HOME}/.ssh:/root/.ssh:ro \
		-v ${shell pwd}/:/work/ \
		${DEV_IMAGE} \
		tail -f /dev/null

run_dev_nix_attach:
	sudo docker exec -it as bash

LOCAL_PATH = $(shell pwd)
SRC_PATH = $(LOCAL_PATH)
# DATA_PATH = $(LOCAL_PATH)/data
# TEST_PATH = $(LOCAL_PATH)/test
TEMP_PATH = $(LOCAL_PATH)/temp
CERT_PATH = $(LOCAL_PATH)/certs
CONTAINER_ARCHI_NAME=archi
CONTAINER_ARCHI_IMAGE=archi-script/archi:latest
# From explore-django
# From archi_helpers - https://gitlab.com/munnox/archi-helpers
build_archi:
	sudo docker build \
		-f containers/archi/Dockerfile \
		-t $(CONTAINER_ARCHI_IMAGE) \
		${LOCAL_PATH}

shell_archi: 
	# @mkdir -p ${DATA_PATH}
	# @mkdir -p ${TEST_PATH}
	@mkdir -p ${TEMP_PATH}
	# -v $(DATA_PATH):/data/:z \
	sudo docker run -it --rm \
		--name $(CONTAINER_ARCHI_NAME)-shell \
		--user $(shell id -u):$(shell id -g) \
		-v $(HOME)/.ssh/:/home/archi/.ssh/ \
		-v $(SRC_PATH)/:/src/ \
		-v $(CERT_PATH):/certs/:z \
		$(CONTAINER_ARCHI_IMAGE) \
			bash
