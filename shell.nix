{
  pkgs ? import <nixpkgs> {}, # here we import the nixpkgs package set
  self ? {packages.aarch64-darwin.unix_config = {__toString= self: "self_unix_config";};}
}:
let
  explore = pkgs.writeScriptBin "explore" ''
    npm run explore
  '';

  archi_help = pkgs.writeScriptBin "archi_help" ''
    # npm run archi_api
    Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --help
  '';

  archi_api = pkgs.writeScriptBin "archi_api" ''
    # npm run archi_api
    Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --script.runScript 'ram/Run API.ajs'
  '';

  archi_test = pkgs.writeScriptBin "archi_test" ''
    # npm run archi_api
    Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --script.runScript 'ram/Testing_Utils.ajs'
  '';

  basepkgs = [
    pkgs.nodejs
    pkgs.openjdk
    pkgs.archi
  ];
  darwin_only = [];
  linux_only = [ ];
  scripthelpers = [
    explore
    archi_help
    archi_api
    archi_test
  ];

  debug = x: pkgs.lib.traceSeq x x;
  # config = ./config/bash;
  # configpath = builtins.toString ./.;
in pkgs.mkShell rec {       # mkShell is a helper function
  name="unix_config_deploy";    # that requires a name
  # And a list of build inputs
  buildInputs = if pkgs.stdenv.isDarwin then 
    basepkgs ++ darwin_only ++ scripthelpers
  else 
    basepkgs ++ linux_only ++ scripthelpers;
  # Then will run this script before give the user the shell
  shellHook = ''
    # export PS1=$txtred"NIX✅$txtrst-"$txtblu"${name}"$txtrst"\n$PS1"
    # echo "Start developing...system = '${pkgs.system}'"                                        
  '';
}