// TODO: Starting a Testing framework in nodejs

const { RAMUtils } = require("../lib/Utils.js");
// import { RAMUtils } from "../lib/Utils.js";

// Testing GridCalc
const test1 = RAMUtils.GridCalc(3);
console.log(`Result RAMUtils.GridCalc(3)=${test1}`);
const test2 = RAMUtils.GridCalc(3, null, 2);
console.log(`Result RAMUtils.GridCalc(3, null, 2)=${test2}`);
const test3 = RAMUtils.GridCalc(3, 2, null);
console.log(`Result RAMUtils.GridCalc(3, 2, null)=${test3}`);