"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// var __SCRIPT_DIR__ = "./";
var console_1 = require("console");
var Utils_js_1 = require("../lib/Utils.js");
// import Utils from "../lib/Utils.js";
// const { RAMUtils } = Utils;
// const { RAMUtils } = require("../lib/Utils.js");
// Object.defineProperty(exports, "__esModule", { value: true });
// import {Point, Box, Bounds, Grid} from "./view_objects.js";
var bounds = new Utils_js_1.Bounds(new Utils_js_1.Point(1, 2), new Utils_js_1.Box(100, 25));
console.log("".concat(bounds));
console.log("Utils grid calc: ".concat(Utils_js_1.RAMUtils.GridCalc(2, null, 4)));
var grid = Utils_js_1.Grid.calcFromCount(3, null, 2);
grid.bounds = new Utils_js_1.Bounds(new Utils_js_1.Point(0, 0), new Utils_js_1.Box(120, 120));
console.log(grid.bounds);
grid.process(function (cell) {
    console.log("".concat(cell.indexes, " -> ").concat(cell.bounds));
});
console.log("Stacked element context");
var parents = [
    1,
    2,
    3
];
var siblings = [
    1,
    2,
    3
];
var children = [
    1,
    2,
    3,
];
// layout parents above siblings then the main element, then place children below.
var overlay_list = [
    parents,
    siblings,
    [null],
    children,
];
var overlay = Utils_js_1.Grid.calcFromCount(overlay_list.length, null, 1);
var overlay_bounds = new Utils_js_1.Bounds(new Utils_js_1.Point(0, 0), new Utils_js_1.Box(500, 1500));
overlay_bounds = new Utils_js_1.Bounds.FromCoordBox(100, 100, 500, 1500);
overlay.bounds = overlay_bounds;
overlay.process(function (level_cell) {
    var overlay_level = overlay_list[level_cell.indexes.index];
    var level_grid = Utils_js_1.Grid.calcFromCount(overlay_level.length, 1, null);
    level_grid.bounds = level_cell.bounds;
    level_grid.process(function (cell) {
        console.log("level ".concat(level_cell.indexes.index, " - index: ").concat(cell.indexes.index, " bounds: ").concat(cell.bounds));
    });
});
// Testing the function below
console.log("");
console.log("DrawGroupWithContents group object with title content nested");
Utils_js_1.RAMArchi.DrawGroupWithContents(null, "test", overlay_bounds, [null, null], 1, null, function (cell) {
    console.log("DrawGroupWithContents ".concat(cell));
    (0, console_1.assert)(cell.extra.nested_flag);
});
console.log("");
console.log("DrawGroupWithContents group object with no text nested to gap at the top");
Utils_js_1.RAMArchi.DrawGroupWithContents(null, "", overlay_bounds, [null, null], 1, null, function (cell) {
    console.log("  Cell: ".concat(cell));
    (0, console_1.assert)(!cell.extra.nested_flag);
});
console.log("");
console.log("DrawGroupWithContents no group object grid layout global coords");
Utils_js_1.RAMArchi.DrawGroupWithContents(null, null, overlay_bounds, [null, null], 1, null, function (cell) {
    console.log("  Cell: ".concat(cell));
    (0, console_1.assert)(!cell.extra.nested_flag);
});
console.log("");
console.log("DrawGroupWithContents group object simple object (simulating a archi element) contents should be nested");
Utils_js_1.RAMArchi.DrawGroupWithContents(null, { test: 1 }, overlay_bounds, [null, null], 1, null, function (cell) {
    console.log("  Cell: ".concat(cell));
    (0, console_1.assert)(cell.extra.nested_flag);
});
console.log("");
console.log("DrawGroupWithContents group object empty group grid layout global coords");
Utils_js_1.RAMArchi.DrawGroupWithContents(null, {}, overlay_bounds, [null, null], 1, null, function (cell) {
    console.log("  Cell: ".concat(cell));
    (0, console_1.assert)(!cell.extra.nested_flag);
});
// Testing the Grid with arrays
overlay_bounds = new Utils_js_1.Bounds.FromCoordBox(0, 0, 400, 400);
console.log("");
console.log("DrawGroupWithContents Nested group with a empty object with multiple nested elements");
Utils_js_1.RAMArchi.DrawGroupWithContents(null, {}, overlay_bounds, [null, null, null, null, null, null, null, null], [40, null, null], [null, 30, null], function (cell) {
    console.log("  Cell: ".concat(cell));
    (0, console_1.assert)(!cell.extra.nested_flag);
});
console.log("Explore Finishing");
