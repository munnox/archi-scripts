"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Grid = exports.GridIndex = exports.CellInfo = exports.Padding = exports.Bounds = exports.Box = exports.Point = void 0;
var Point = /** @class */ (function () {
    function Point(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = 0; }
        this.x = x;
        this.y = y;
    }
    Point.prototype.toString = function () {
        return "Point(x=".concat(this.x.toFixed(3), ", y=").concat(this.y.toFixed(3), ")");
    };
    return Point;
}());
exports.Point = Point;
// TODO Feels like it needs an abstraction for Width and Height for a box layout
// But a box model feels wrong right now and too much like how HTML does it...
// Need thinking about
var Box = /** @class */ (function () {
    function Box(width, height) {
        this.width = width;
        this.height = height;
    }
    Box.prototype.toString = function () {
        var s;
        var width = this.width ? this.width.toFixed(2) : this.width;
        var height = this.height ? this.height.toFixed(3) : this.height;
        return "Box(w=".concat(width, ", h=").concat(height, ")");
    };
    return Box;
}());
exports.Box = Box;
var Bounds = /** @class */ (function () {
    function Bounds(point, box) {
        this.point = point;
        this.box = box;
    }
    Object.defineProperty(Bounds.prototype, "x", {
        get: function () {
            return this.point.x;
        },
        set: function (value) {
            this.point.x = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Bounds.prototype, "y", {
        get: function () {
            return this.point.y;
        },
        set: function (value) {
            this.point.y = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Bounds.prototype, "width", {
        get: function () {
            return this.box.width;
        },
        set: function (value) {
            this.box.width = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Bounds.prototype, "height", {
        get: function () {
            return this.box.height;
        },
        set: function (value) {
            this.box.height = value;
        },
        enumerable: false,
        configurable: true
    });
    Bounds.prototype.toString = function () {
        return "Bounds(p=".concat(this.point, ", b=").concat(this.box, ")");
    };
    /**
     * Create a nested boundry with nested flag to give it a larger top margin
     * @param padding A padding object
     * @param group_nesting A flag to push the top padding down to give room for nesting in group element
     * @param unit The base unit size
     * @returns
     */
    Bounds.prototype.nested = function (padding, group_nesting, unit) {
        if (group_nesting === void 0) { group_nesting = false; }
        if (unit === void 0) { unit = 12; }
        if (!padding) {
            padding = Padding.FromSingle(unit, group_nesting);
        }
        var nested_bounds = new Bounds(new Point(this.x + padding.left, this.y + padding.top), new Box(this.width - (padding.left + padding.right), this.height - (padding.top + padding.bottom)));
        // console.log(`Nested Boundry padding=${padding}, parent=${this}, nested=${nested_bounds}, group_nest=${group_nest}`);
        return nested_bounds;
    };
    Bounds.FromCoordBox = function (x, y, width, height) {
        return new Bounds(new Point(x, y), new Box(width, height));
    };
    Bounds.Clone = function (bound) {
        return new Bounds(new Point(bound.point.x, bound.point.y), new Box(bound.box.width, bound.box.height));
    };
    Bounds.CloneAndResetOrigin = function (bounds) {
        var cloned_bounds = Bounds.Clone(bounds);
        cloned_bounds.x = 0;
        cloned_bounds.y = 0;
        return cloned_bounds;
    };
    return Bounds;
}());
exports.Bounds = Bounds;
var Padding = /** @class */ (function () {
    function Padding(top, bottom, left, right, framev, frameh) {
        this.top = top,
            this.bottom = bottom,
            this.left = left,
            this.right = right,
            this.framev = framev,
            this.frameh = frameh;
    }
    Padding.FromSingle = function (unit, group_nesting, top_padding_multiplier, min_top_padding) {
        if (unit === void 0) { unit = 12; }
        if (group_nesting === void 0) { group_nesting = false; }
        if (top_padding_multiplier === void 0) { top_padding_multiplier = 1.5; }
        if (min_top_padding === void 0) { min_top_padding = 16; }
        var padding = new Padding(unit, unit, unit, unit, unit / 2, // gap between columns
        unit / 2);
        // If a group nested object then give most top margin
        if (group_nesting) {
            var top_padding = Math.round(unit * top_padding_multiplier);
            if (top_padding < min_top_padding) {
                top_padding = min_top_padding;
            }
            padding.top = top_padding;
        }
        return padding;
    };
    Padding.prototype.toString = function () {
        return "Padding(top=".concat(this.top.toFixed(2), ", bottom=").concat(this.bottom.toFixed(2), ", left=").concat(this.left.toFixed(2), ", right=").concat(this.right.toFixed(2), ", framev=").concat(this.framev.toFixed(2), ", frameh=").concat(this.frameh.toFixed(2), ")");
    };
    return Padding;
}());
exports.Padding = Padding;
var CellInfo = /** @class */ (function () {
    // nested_flag: boolean;
    // view: any | null;
    // name: string | null;
    function CellInfo(indexes, bounds, padding, element, parent_element, extra) {
        this.indexes = indexes;
        this.bounds = bounds;
        this.padding = padding;
        this.element = element;
        this.parent_element = parent_element;
        this.extra = extra;
    }
    CellInfo.prototype.toString = function () {
        return "Cell(indexes=".concat(this.indexes, ", bounds=").concat(this.bounds, ", padding=").concat(this.padding, ", element=").concat(this.element, ", parent=").concat(this.parent_element, ")");
    };
    return CellInfo;
}());
exports.CellInfo = CellInfo;
var GridIndex = /** @class */ (function () {
    function GridIndex(index, row, column) {
        this.index = index;
        this.row = row;
        this.column = column;
    }
    GridIndex.prototype.toString = function () {
        return "GridIndex(T=".concat(this.index, ", R=").concat(this.row, ", C=").concat(this.column, ")");
    };
    return GridIndex;
}());
exports.GridIndex = GridIndex;
/**
 * A Class to represent a grid object.
 *
 * This can be used to layout items in rows and columns with spacing around them.
 */
var Grid = /** @class */ (function () {
    function Grid(rows, columns, bounds, padding, unit) {
        if (bounds === void 0) { bounds = null; }
        if (padding === void 0) { padding = null; }
        if (unit === void 0) { unit = 12; }
        // Calculate the Row array
        this.row_array = null;
        if (Array.isArray(rows)) {
            this.row_array = rows;
            // this.rows = rows.length;
        }
        else if (Number.isFinite(rows)) {
            this.row_array = [];
            for (var i = 0; i < rows; i++) {
                this.row_array.push(null);
            }
        }
        else if (rows == null) {
            this.row_array = null;
        }
        // Calculate the Column array
        this.column_array = null;
        if (Array.isArray(columns)) {
            this.column_array = columns;
        }
        else if (Number.isFinite(columns)) {
            this.column_array = [];
            for (var i = 0; i < columns; i++) {
                this.column_array.push(null);
            }
        }
        else if (columns == null) {
            this.column_array = null;
        }
        this.bounds = bounds;
        this.padding = padding;
        this._cell = null;
        this._unit = unit;
        // TODO: Convert from a number to a list of null or ints
        // for the column and row widths.
        // Starting with the following...
        // let row_array = [];
        // for (let i=0;i<rows;rows++) {
        //     row_array.push(null);
        // }
        // let col_array = [];
        // for (let i=0;i<columns;rows++) {
        //     col_array.push(null);
        // }
    }
    Object.defineProperty(Grid.prototype, "row_count", {
        get: function () {
            if (this.row_array !== null) {
                return this.row_array.length;
            }
            return null;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "column_count", {
        get: function () {
            if (this.column_array !== null) {
                return this.column_array.length;
            }
            return null;
        },
        enumerable: false,
        configurable: true
    });
    Grid.prototype.toString = function () {
        return "Grid(rows=".concat(this.row_count, ", columns=").concat(this.column_count, ")");
    };
    Object.defineProperty(Grid.prototype, "bounds", {
        get: function () {
            return this._bounds;
        },
        set: function (value) {
            // console.log(`Grid Bounds set: ${value}`);
            // console.log(`Grid Bounds set: ${value}`);
            this._bounds = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "padding", {
        get: function () {
            return this._padding;
        },
        set: function (value) {
            this._padding = value;
        },
        enumerable: false,
        configurable: true
    });
    /*
     * Process each element of a grid with a cell boundry indexs for total row and column.
    */
    Grid.prototype.process = function (element_callback) {
        // console.log(`Bounds: ${this._bounds}`);
        return Grid.process(this.row_array, this.column_array, this._bounds, element_callback, this._padding, this._unit, this._group_nesting);
    };
    /*
     * Statically Process each element of a grid with a cell boundry indexs for total row and column.
    */
    Grid.process = function (row_array, column_array, bounds, element_callback, padding, unit, group_nesting) {
        if (padding === void 0) { padding = null; }
        if (unit === void 0) { unit = 12; }
        if (group_nesting === void 0) { group_nesting = true; }
        if (!padding) {
            padding = Padding.FromSingle(unit, group_nesting);
            // console.log(`New padding: ${padding}`)
        }
        var nested_bounds = bounds.nested(padding, group_nesting);
        // figure the amount of the grid manually defined
        // deal with height and rows here
        var total_defined_height = 0;
        var total_undefined_rows = 0;
        row_array.forEach(function (row) {
            if (row === null) {
                total_undefined_rows += 1;
            }
            else {
                total_defined_height += row;
            }
        });
        // figure the amount of the grid manually defined
        // deal with width and columns here
        var total_defined_width = 0;
        var total_undefined_columns = 0;
        column_array.forEach(function (col) {
            if (col === null) {
                total_undefined_columns += 1;
            }
            else {
                total_defined_width += col;
            }
        });
        // console.log(`Show info row ${total_defined_height}, ${total_undefined_rows}`)
        // console.log(`Show info col ${total_defined_width}, ${total_undefined_columns}`)
        // Compute cell size based on the nested boundry and grid defined
        var row_undefined_size = nested_bounds.box.height - padding.frameh * (row_array.length - 1) - total_defined_height;
        var row_height = row_undefined_size / total_undefined_rows;
        var column_undefined_size = nested_bounds.box.width - padding.framev * (column_array.length - 1) - total_defined_width;
        var column_width = column_undefined_size / total_undefined_columns;
        // console.log(`Grid Process: this=${this}, Padding=${padding}, boundry=${bounds}, nested_bounds=${nested_bounds}, Cell size=${JSON.stringify({row_height, column_width})}`);
        // start to define array with column width and set them to the default width for now
        // This will allow the setting fix widths
        var local_row_array = row_array.map(function (r) {
            if (r === null) {
                return row_height;
            }
            return r;
        });
        var local_column_array = column_array.map(function (r) {
            if (r === null) {
                return column_width;
            }
            return r;
        });
        // Loop through the grid cells
        for (var index_row = 0; index_row < row_array.length; index_row++) {
            for (var index_column = 0; index_column < column_array.length; index_column++) {
                // console.log(`${row}, ${column} ${JSON.stringify(this)}`)
                var width_sum = nested_bounds.point.x;
                for (var i = 0; i < index_column; i++) {
                    width_sum += local_column_array[i] + padding.framev;
                }
                var height_sum = nested_bounds.point.y;
                for (var i = 0; i < index_row; i++) {
                    height_sum += local_row_array[i] + padding.frameh;
                }
                var cell_bounds = new Bounds(new Point(width_sum, height_sum), new Box(local_column_array[index_column], local_row_array[index_row]));
                var index_total = index_column + column_array.length * index_row;
                var indexes = new GridIndex(index_total, index_row, index_column);
                // let returned_bounds = 
                var cell = new CellInfo(indexes, cell_bounds, padding, null, null, null);
                element_callback(cell);
            }
        }
        return nested_bounds;
    };
    Grid.prototype.setCellSize = function (width, height) {
        this._cell = new Box(width, height);
    };
    Grid.prototype.getGridBox = function (cell_bounds) {
        this.setCellSize(cell_bounds.width, cell_bounds.height);
        return new Box(this._cell.width * this.column_count, this._cell.height * this.row_count);
    };
    /**
     * Calculate the grid with defined row or columns and a given number of element to display.
    */
    Grid.calcFromCount = function (count, rows, columns) {
        if (rows === void 0) { rows = null; }
        if (columns === void 0) { columns = null; }
        if ((rows === null) && (columns !== null)) {
            // if rows are null aqnd col is a number then calc rows
            // console.log(`Grid calc init total=${count} row=${rows} cols=${columns}`);
            if (Array.isArray(columns)) {
                rows = Math.ceil(count / columns.length);
            }
            else {
                rows = Math.ceil(count / columns);
                // console.log(`Grid calc row null final total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
            }
        }
        else if ((rows !== null) && (columns === null)) {
            // if rows is fixed calulate cols
            // console.log(`Grid calc init total=${count} row=${rows} cols=${columns}`);
            if (Array.isArray(rows)) {
                columns = Math.ceil(count / rows.length);
            }
            else {
                columns = Math.ceil(count / rows);
                // console.log(`Grid calc columns null final total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
            }
        }
        else if ((rows !== null) && (columns === null)) {
            // if no grid is defined then fix columns to 1
            // list itterate rows
            // console.log(`Grid calc no grid size defined else total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
            columns = 1;
            rows = Math.ceil(count / columns);
        }
        return new Grid(rows, columns);
    };
    return Grid;
}());
exports.Grid = Grid;
// console.log(`Exports view_objects: ${Object.keys(exports)}`)
