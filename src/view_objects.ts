import { AnyARecord } from "dns";

export class Point {
    x: number;
    y: number;

    constructor(x: number=0, y: number=0) {
        this.x = x;
        this.y = y;
    }

    toString() {
        return `Point(x=${this.x.toFixed(3)}, y=${this.y.toFixed(3)})`;
    }
}

// TODO Feels like it needs an abstraction for Width and Height for a box layout
// But a box model feels wrong right now and too much like how HTML does it...
// Need thinking about
export class Box {
    width: number;
    height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

    toString() {
        let s: String;
        let width = this.width ? this.width.toFixed(2): this.width;
        let height = this.height ? this.height.toFixed(3): this.height;
        return `Box(w=${width}, h=${height})`;
    }
}

export class Bounds {
    point: Point;
    box: Box;

    constructor(point: Point, box: Box) {
        this.point = point;
        this.box = box;
    }

    public get x() {
        return this.point.x;
    }
    public set x(value) {
        this.point.x = value;
    }

    public get y() {
        return this.point.y;
    }
    public set y(value) {
        this.point.y = value;
    }

    public get width() {
        return this.box.width;
    }
    public set width(value) {
        this.box.width = value;
    }

    public get height() {
        return this.box.height;
    }
    public set height(value) {
        this.box.height = value;
    }

    toString() {
        return `Bounds(p=${this.point}, b=${this.box})`;
    }

    /**
     * Create a nested boundry with nested flag to give it a larger top margin
     * @param padding A padding object
     * @param group_nesting A flag to push the top padding down to give room for nesting in group element
     * @param unit The base unit size
     * @returns
     */
    nested(padding: Padding, group_nesting: boolean=false, unit: number=12): Bounds {

        if (!padding) {
            padding = Padding.FromSingle(unit, group_nesting);
        }

        let nested_bounds = new Bounds(
            new Point(
                this.x + padding.left,
                this.y + padding.top
            ),
            new Box(
                this.width - (padding.left + padding.right),
                this.height - (padding.top + padding.bottom)
            )
        );
        // console.log(`Nested Boundry padding=${padding}, parent=${this}, nested=${nested_bounds}, group_nest=${group_nest}`);
        return nested_bounds;
    }

    static FromCoordBox(x: number, y: number, width: number, height: number): Bounds {
        return new Bounds(new Point(x,y,), new Box(width, height));
    }

    static Clone(bound: Bounds) {
        return new Bounds(
            new Point(bound.point.x, bound.point.y),
            new Box(bound.box.width, bound.box.height)
        );
    }

    static CloneAndResetOrigin(bounds) {
        let cloned_bounds = Bounds.Clone(bounds);
        cloned_bounds.x = 0;
        cloned_bounds.y = 0;
        return cloned_bounds;
    }
}

export class Padding {
    top: number; // top margin
    bottom: number; // bottom margin
    left: number; // Left margin
    right: number; // Right margin 
    framev: number; // gap between columns
    frameh: number; // gap between rows

    constructor(
        top: number, bottom: number,
        left: number, right: number,
        framev: number, frameh: number
    ) {
        this.top = top,
        this.bottom = bottom,

        this.left = left,
        this.right = right,
        
        this.framev = framev,
        this.frameh = frameh
    }

    static FromSingle(
        unit=12,
        group_nesting: boolean=false,
        top_padding_multiplier: number=1.5,
        min_top_padding: number=16
    ): Padding {
        let padding = new Padding(
            unit,
            unit,
            unit,
            unit,
            unit/2, // gap between columns
            unit/2, // gap between rows
        );
        // If a group nested object then give most top margin
        if (group_nesting) {
            let top_padding = Math.round(unit*top_padding_multiplier);
            if (top_padding < min_top_padding) {
                top_padding = min_top_padding;
            }
            padding.top = top_padding;
        }
        return padding;
    }

    toString() {
        return `Padding(top=${this.top.toFixed(2)}, bottom=${this.bottom.toFixed(2)}, left=${this.left.toFixed(2)}, right=${this.right.toFixed(2)}, framev=${this.framev.toFixed(2)}, frameh=${this.frameh.toFixed(2)})`
    }
}

export class CellInfo {
    indexes: GridIndex;
    bounds: Bounds;
    element: any | null;
    parent_element: any | null;
    padding: Padding | null;
    extra: any | null;
    // nested_flag: boolean;
    // view: any | null;
    // name: string | null;

    constructor(
        indexes: GridIndex,
        bounds: Bounds,
        padding: Padding,
        element: any,
        parent_element: any,
        extra: any
    ) {
        this.indexes = indexes;
        this.bounds = bounds;
        this.padding = padding;
        this.element = element;
        this.parent_element = parent_element;
        this.extra = extra;
    }

    toString() {
        return `Cell(indexes=${this.indexes}, bounds=${this.bounds}, padding=${this.padding}, element=${this.element}, parent=${this.parent_element})`;
    }
}

export class GridIndex {
    index: number;
    row: number;
    column: number;

    constructor(index, row, column) {
        this.index = index;
        this.row = row;
        this.column = column;
    }

    toString() {
        return `GridIndex(T=${this.index}, R=${this.row}, C=${this.column})`;
    }
}

/**
 * A Class to represent a grid object.
 * 
 * This can be used to layout items in rows and columns with spacing around them.
 */
export class Grid {
    // Row array of column number or null for auto size
    row_array: Array<number| null> | null;
    // Column array of column number or null for auto size
    column_array: Array<number| null> | null;
    // A cell boundry size
    _cell: Box;
    
    // Setting the bounds, padding, default unit, and group_nesting for the grid
    _bounds: Bounds;
    _padding: Padding | null;
    _unit: number;
    _group_nesting: boolean;

    constructor(
        rows: number | Array<number| null> | null,
        columns: number | Array<number| null> | null,
        bounds: Bounds | null = null,
        padding: Padding | null = null,
        unit: number = 12
    ) {

        // Calculate the Row array
        this.row_array = null;
        if (Array.isArray(rows)) {
            this.row_array = rows;
            // this.rows = rows.length;
        } else if (Number.isFinite(rows)) {
            this.row_array = [];
            for (let i=0;i<rows;i++) {
                this.row_array.push(null);
            }
        } else if (rows == null) {
            this.row_array = null;
        }

        // Calculate the Column array
        this.column_array = null;
        if (Array.isArray(columns)) {
            this.column_array = columns;
        } else if (Number.isFinite(columns)) {
            this.column_array = [];
            for (let i=0;i<columns;i++) {
                this.column_array.push(null);
            }
        } else if (columns == null) {
            this.column_array = null;
        }


        this.bounds = bounds;

        this.padding = padding;

        this._cell = null;
        this._unit = unit;

        // TODO: Convert from a number to a list of null or ints
        // for the column and row widths.
        // Starting with the following...

        // let row_array = [];
        // for (let i=0;i<rows;rows++) {
        //     row_array.push(null);
        // }
        // let col_array = [];
        // for (let i=0;i<columns;rows++) {
        //     col_array.push(null);
        // }
    }

    public get row_count() {
        if (this.row_array !== null) {
            return this.row_array.length;
        }
        return null;
    }

    public get column_count() {
        if (this.column_array !== null) {
            return this.column_array.length;
        }
        return null;
    }

    toString(): string {
        return `Grid(rows=${this.row_count}, columns=${this.column_count})`;
    }

    public set bounds(value) {
        // console.log(`Grid Bounds set: ${value}`);
        // console.log(`Grid Bounds set: ${value}`);
        this._bounds = value;
    }

    public get bounds() {
        return this._bounds;
    }

    public set padding(value) {
        this._padding = value;
    }

    public get padding() {
        return this._padding;
    }

    /*
     * Process each element of a grid with a cell boundry indexs for total row and column.
    */
    public process(
        element_callback: (cell: CellInfo) => void
    ) {
        // console.log(`Bounds: ${this._bounds}`);
        return Grid.process(
            this.row_array, this.column_array,
            this._bounds,
            element_callback,
            this._padding,
            this._unit,
            this._group_nesting
        );
    }

    /*
     * Statically Process each element of a grid with a cell boundry indexs for total row and column.
    */
    static process(
        row_array: Array<number| null> | null,
        column_array: Array<number| null> | null,
        bounds: Bounds,
        element_callback: (cell: CellInfo) => void,
        padding: Padding | null = null,
        unit: number = 12,
        group_nesting: boolean = true
    ) {
        if (!padding) {
            padding = Padding.FromSingle(unit, group_nesting);
            // console.log(`New padding: ${padding}`)
        }

        let nested_bounds = bounds.nested(padding, group_nesting);

        // figure the amount of the grid manually defined
        // deal with height and rows here
        let total_defined_height = 0;
        let total_undefined_rows = 0;
        row_array.forEach(row => {
            if (row === null) {
                total_undefined_rows += 1;
            } else {
                total_defined_height += row;
            }
        })

        // figure the amount of the grid manually defined
        // deal with width and columns here
        let total_defined_width = 0;
        let total_undefined_columns = 0;
        column_array.forEach(col => {
            if (col === null) {
                total_undefined_columns += 1;
            } else {
                total_defined_width += col;
            }
        })
        // console.log(`Show info row ${total_defined_height}, ${total_undefined_rows}`)
        // console.log(`Show info col ${total_defined_width}, ${total_undefined_columns}`)

        // Compute cell size based on the nested boundry and grid defined
        const row_undefined_size = nested_bounds.box.height - padding.frameh * (row_array.length - 1) - total_defined_height;
        const row_height = row_undefined_size / total_undefined_rows;

        const column_undefined_size = nested_bounds.box.width - padding.framev * (column_array.length - 1) - total_defined_width;
        const column_width = column_undefined_size / total_undefined_columns;

        // console.log(`Grid Process: this=${this}, Padding=${padding}, boundry=${bounds}, nested_bounds=${nested_bounds}, Cell size=${JSON.stringify({row_height, column_width})}`);

        // start to define array with column width and set them to the default width for now
        // This will allow the setting fix widths
        let local_row_array = row_array.map(r => {
            if (r === null) {
                return row_height;
            }
            return r;
        });
        let local_column_array = column_array.map(r => {
            if (r === null) {
                return column_width;
            }
            return r;
        });

        // Loop through the grid cells
        for (let index_row = 0; index_row < row_array.length; index_row++) {
            for (let index_column = 0; index_column < column_array.length; index_column++) {
                // console.log(`${row}, ${column} ${JSON.stringify(this)}`)
                let width_sum = nested_bounds.point.x;
                for (let i=0;i<index_column;i++) {
                    width_sum += local_column_array[i] + padding.framev;
                }
                let height_sum = nested_bounds.point.y;
                for (let i=0;i<index_row;i++) {
                    height_sum += local_row_array[i] + padding.frameh;
                }

                let cell_bounds = new Bounds(
                    new Point(
                        width_sum,
                        height_sum
                    ),
                    new Box(
                        local_column_array[index_column],
                        local_row_array[index_row]
                    ),
                );
                let index_total = index_column + column_array.length * index_row;
                let indexes = new GridIndex(
                    index_total,
                    index_row,
                    index_column
                )
                // let returned_bounds = 
                let cell = new CellInfo(
                    indexes,
                    cell_bounds,
                    padding,
                    null,
                    null,
                    null
                );
                element_callback(cell);

            }
        }

        return nested_bounds;
    }


    setCellSize(width, height) {
        this._cell = new Box(
            width,
            height
        )
    }

    getGridBox(cell_bounds: Bounds) {
        this.setCellSize(cell_bounds.width, cell_bounds.height);
        return new Box(
            this._cell.width*this.column_count,
            this._cell.height*this.row_count
        )
    }

    /**
     * Calculate the grid with defined row or columns and a given number of element to display.
    */
    static calcFromCount(
        count,
        rows: number | Array<number | null> | null = null,
        columns: number | Array<number | null> | null = null
    ) {

        if ((rows === null) && (columns !== null)) {
            // if rows are null aqnd col is a number then calc rows
            // console.log(`Grid calc init total=${count} row=${rows} cols=${columns}`);
            if (Array.isArray(columns)) {
                rows = Math.ceil(count / columns.length);
            } else {
                rows = Math.ceil(count / columns);
                // console.log(`Grid calc row null final total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
            }
        } else if ((rows !== null) && (columns === null)) {
            // if rows is fixed calulate cols
            // console.log(`Grid calc init total=${count} row=${rows} cols=${columns}`);
            if (Array.isArray(rows)) {
                columns = Math.ceil(count / rows.length);
            } else {
                columns = Math.ceil(count / rows);
                // console.log(`Grid calc columns null final total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
            }
        } else if ((rows !== null) && (columns === null)) {
            // if no grid is defined then fix columns to 1
            // list itterate rows
            // console.log(`Grid calc no grid size defined else total=${count} row=${rows} cols=${columns} total=${rows*columns}`);
            columns = 1;
            rows = Math.ceil(count / columns);
        }
        return new Grid(rows, columns);
    }
}

// console.log(`Exports view_objects: ${Object.keys(exports)}`)
