// var __SCRIPT_DIR__ = "./";
import { assert } from "console";
import { RAMArchi, RAMUtils, Grid, Bounds, Point, Box } from "../lib/Utils.js";
// import Utils from "../lib/Utils.js";
// const { RAMUtils } = Utils;

// const { RAMUtils } = require("../lib/Utils.js");

// Object.defineProperty(exports, "__esModule", { value: true });

// import {Point, Box, Bounds, Grid} from "./view_objects.js";

let bounds = new Bounds(new Point(1, 2), new Box(100,25));
console.log(`${bounds}`);
console.log(`Utils grid calc: ${RAMUtils.GridCalc(2, null, 4)}`);

let grid = Grid.calcFromCount(3, null, 2);
grid.bounds = new Bounds(new Point(0,0), new Box(120, 120));
console.log(grid.bounds);
grid.process(
    (cell) => {
        console.log(`${cell.indexes} -> ${cell.bounds}`);
    }
)

console.log("Stacked element context");

let parents = [
    1,
    2,
    3
]
let siblings = [
    1,
    2,
    3
]
let children = [
    1,
    2,
    3,
]

// layout parents above siblings then the main element, then place children below.
let overlay_list = [
    parents,
    siblings,
    [null],
    children,
]

let overlay = Grid.calcFromCount(overlay_list.length, null, 1);

let overlay_bounds = new Bounds(
    new Point( 0, 0),
    new Box(500, 1500)
);
overlay_bounds = new Bounds.FromCoordBox(
    100, 100,
    500, 1500
);
overlay.bounds = overlay_bounds;
overlay.process(
    (level_cell) => {
        let overlay_level = overlay_list[level_cell.indexes.index];
        let level_grid = Grid.calcFromCount(overlay_level.length, 1, null);
        level_grid.bounds = level_cell.bounds;
        level_grid.process(
            (cell) => {
                console.log(`level ${level_cell.indexes.index} - index: ${cell.indexes.index} bounds: ${cell.bounds}`);
            })
    })

// Testing the function below
console.log("");
console.log(`DrawGroupWithContents group object with title content nested`);
RAMArchi.DrawGroupWithContents(null, "test", overlay_bounds, [null,null], 1,null,
    (cell) => {
        console.log(`DrawGroupWithContents ${cell}`);
        assert(cell.extra.nested_flag);
    }
);

console.log("");
console.log(`DrawGroupWithContents group object with no text nested to gap at the top`);
RAMArchi.DrawGroupWithContents(null, "", overlay_bounds, [null,null], 1,null,
    (cell) => {
        console.log(`  Cell: ${cell}`);
        assert(!cell.extra.nested_flag);
    }
);

console.log("");
console.log(`DrawGroupWithContents no group object grid layout global coords`);
RAMArchi.DrawGroupWithContents(null, null, overlay_bounds, [null,null], 1,null,
    (cell) => {
        console.log(`  Cell: ${cell}`);
        assert(!cell.extra.nested_flag);
    }
);

console.log("");
console.log(`DrawGroupWithContents group object simple object (simulating a archi element) contents should be nested`);
RAMArchi.DrawGroupWithContents(null, {test: 1}, overlay_bounds, [null,null], 1,null,
    (cell) => {
        console.log(`  Cell: ${cell}`);
        assert(cell.extra.nested_flag);
    }
);

console.log("");
console.log(`DrawGroupWithContents group object empty group grid layout global coords`);
RAMArchi.DrawGroupWithContents(null, {}, overlay_bounds, [null,null], 1,null,
    (cell) => {
        console.log(`  Cell: ${cell}`);
        assert(!cell.extra.nested_flag);
    }
);

// Testing the Grid with arrays
overlay_bounds = new Bounds.FromCoordBox(
    0, 0,
    400, 400
);
console.log("");
console.log("DrawGroupWithContents Nested group with a empty object with multiple nested elements");
RAMArchi.DrawGroupWithContents(null, {}, overlay_bounds, [null,null,null,null,null,null,null,null], [ 40, null, null ], [null, 30, null],
    (cell) => {
        console.log(`  Cell: ${cell}`);
        assert(!cell.extra.nested_flag);
    }
);

console.log("Explore Finishing");