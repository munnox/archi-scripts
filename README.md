# Helper scripts for Archi

Useful scripts for Archi's plugin Jarchi.

## Useful links

Further information to learn more about Jarchi:


* Main plugin - https://github.com/archimatetool/archi-scripting-plugin

* Scripting wiki - https://github.com/archimatetool/archi-scripting-plugin/wiki

* GraalVM Javascript
    * https://www.graalvm.org/latest/reference-manual/js/JavaScriptCompatibility/
    * https://www.graalvm.org/latest/reference-manual/js/JavaInteroperability/
    * https://www.graalvm.org/latest/reference-manual/js/JavaInteroperability/#extending-java-classes
  

* More Github GISTs can be found using the following query: https://gist.github.com/search?utf8=%E2%9C%93&q=%23jarchi+extension%3Aajs&ref=searchresults

## Contributed Scripts

* https://github.com/archimatetool/archi-scripting-plugin/wiki/Community-shared-scripts

    * Older community script with some useful utility and example.
        * https://github.com/archi-contribs/jarchi-community-script-pack
    * Collected scripts from GISTs.
        * https://github.com/rchevallier/jarchi-lib
    * Github gist query looking for `#jarchi` and `*.ajs`.
        * https://gist.github.com/search?utf8=%E2%9C%93&q=%23jarchi+extension%3Aajs&ref=searchresults

## Useful Libaries

* PapaParse - https://www.papaparse.com/
* MyExcel - https://github.com/josesegarra/MyExcel
* Jexcel - https://jspreadsheets.com/jexcel/ - Display datasheet
* Jsuites - https://github.com/jsuites/jsuites
* D3 Visualisation - https://d3js.org/ https://github.com/d3/d3
  * https://www.d3indepth.com/force-layout/
* Vis.js - browser based vis - https://visjs.org/


## Interesting Gists

* https://gist.github.com/ThomasRohde/8daf0a5c5814b597953983d0a549b04c <- Browser integration
* https://gist.github.com/smileham/88b8e5fd257bd6ce08e78230c951f1a2 <- Another Extrernal command runner reinforceing another way to run commands.

# Running the Archi from the CLI

```bash
Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --help

# Jarchi
Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --script.runScript "ram/Simple View.ajs" -myArg "HelloWord"
```

# Installing Archi on linux

## Getting Archi via Nix

```bash
nix-shell -p archi
```

## Download and install Archi

```bash
curl -O arch.tar.gz https://www.archimatetool.com/downloads/archi/5.4.2/Archi-Linux64-5.4.2.tgz

sudo tar -xzfv arch.tar.gz ~/Archi_test

ln -s ~/Archi_test/bin/Archi ~/

```

# Using the Typescript system

Need Docker - Container for development with nix and dependancies within.

Or

Need NodeJS and npm or now just docker.

## Docker dependancy:

```bash
# Build a docker container to develop in
make build_dev_nix
# Run the development container locally
make run_dev_nix

# from within the dev container
nix-shell
```

## Old methods of the running:

Using nodejs or npm:

```
# Using Nix explicitly
nix-shell -p nodejs

# Compile and Run explore tyepcript
npm run explore

# Run the Archi scripts from the terminal
## either
npm run archi_api
## or via nix
archi_api
```

To use ES2015+ it need `"type": "module",` to the `package.json`. Also the lines in the `tsconfig.json` need to be uncommented.

Right now it producing old style Javascript which allow the view_objects to be used in archi.

As `Export` cannot seem to be used within the GraalVM environment.

# Using Javascript testing

```bash
nix-shell -p nodejs
node tests/Utility_Tests.js
```

# Working with the Database plugin

Investigation of Azure Data Studio Operating on Archi Database export

Plugin https://github.com/archi-contribs/database-plugin

DB Structure https://github.com/archi-contribs/database-plugin/wiki/3.-The-database-structure

This works how ever has a few inconsistancies need to monitor the stability