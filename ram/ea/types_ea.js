class TypeUtils {
    static isType(type, element) {
        // console.log(`Test ${type.element_type} ?= ${element.type}`);
        // console.log(`Test ${type.element_specialization} ?= ${element.specialization}`);
        if ((type.element_type === element.type) && (type.element_specialization === element.specialization)) {
            return true;
        }
        return false;
    }
}

class Supplier {
    static element_type = "grouping"
    static element_specialization = "Supplier"
    static element_properties = [
        // ["Lifecycle State", "String|Linked", null], // The Lifecycle State of the Supplier this is not a direct property but could/should be a link or links in the graph
    ]
}

class External {
    static element_type = "grouping"
    static element_specialization = "External"
    static element_fillcolor = "#800000"
    static element_properties = [
        ["Owner", "String", ""], // Tie break owner to solve disputes and who ultimatly is responsible for the company
        ["Level", "Float", 1], // The heirarchy level of the capablity
    ]
}
class Domain {
    static element_type = "grouping"
    static element_specialization = "Domain"
    static element_fillcolor = "#ff8000"
    static element_properties = [
        // ["Lifecycle State", "String|Linked", null], // The Lifecycle State of the Domain this is not a direct property but could/should be a link or links in the graph
        ["Owner", "String", ""], // Tie break owner to solve disputes and who ultimatly is responsible for the company
        ["Level", "Float", 1], // The heirarchy level of the capablity
    ]
}

class Capability_Domain {
    static element_type = "capability"
    static element_specialization = "Capability Domain"
    static element_fillcolor = null
    static element_properties = [
        ["Owner", "String", ""], // Tie break owner to solve disputes and who ultimatly is responsible for the Capability Group
    ]
}

class Capability {
    static element_type = "capability"
    static element_specialization = "Selected Capability"
    static element_fillcolor = null
    static element_properties = [
        // ["Lifecycle State", "String|Linked", null], // The Lifecycle State of the Capability this is not a direct property but could/should be a link or links in the graph
        ["Owner", "String", ""], // Tie break owner to solve disputes and who ultimatly is responsible for the Capability
        // ["Maturity", "String"], // removed as I do not feel a Capability really has a maturity
        ["Level", "Float", 1], // The "tech" level of the capablity
        ["Realisation", "Float", 0], // How realised the capability currently is
        ["Target Realisation", "Float", 0], // How realised the capability should be (Maybe an edge case)
        ["Importance", "Float", 0] // How important is this Capablitiy for the business
    ]
}

class Solution {
    static element_type = "grouping"
    static element_specialization = "Solution"
    static element_fillcolor = "#408080"
    static element_properties = [
        // ["Lifecycle State", "String|Linked", null], // The Lifecycle State of the Solution this is not a direct property but could/should be a link or links in the graph
        ["Owner", "String", ""], // Tie break owner to solve disputes and who ultimatly is responsible for the solution
        ["Business Owner", "String", ""], // Owns its place in the business, processes and use
        ["Technical Owner", "String", ""], // Owns the technical function support, backups and functionalitya of the assets and interfaces
        ["Adaptability", "Float", 0], // How adaptable the Solution is, a measure of flexability couples to migration costs
        ["Maturity", "Float", 0], // How well tried and tested is the Solution - coupled to how reliable is it for doing its job
        ["Adoption", "Float", 0], // How well adopted is the Solution accross the business
        ["Internal Support", "Float", 0], // How much internal support is there in the business? <- does the business have answers
        ["External Support", "Float", 0], // How much external support is there in the world? <- can answers be found
        ["Cost.Low.Running", "Float", 0], // Cost to run the solution not including Solutions is is costs with the process - three point for a triangle distribution
        ["Cost.Med.Running", "Float", 0], // Middle range for this number
        ["Cost.High.Running", "Float", 0], // Upper range for this number
    ]
}

class Asset {
    static element_type = "grouping"
    static element_specialization = "Asset"
    static element_fillcolor = "#00ff00"
    static element_properties = [
        // ["Lifecycle State", "String|Linked", null], // The Lifecycle State of the Asset this is not a direct property but could/should be a link or links in the graph
        ["Owner", "String", ""], // Tie break owner to solve disputes and who ultimatly is responsible for the asset
        ["Business Owner", "String", ""], // Owns its place in the business, processes and use
        ["Technical Owner", "String", ""], // Owns the technical function support, backups and functionality
        ["Adaptability", "Float", 0], // How adaptable the asset is, a measure of flexability couples to migration costs
        ["Maturity", "Float", 0], // How well tried and tested is the asset - coupled to how reliable is it for doing its job
        ["Adoption", "Float", 0], // How well adopted is the Asset accross the business
        // GARNER TIME Evaluation
        // https://www.leanix.net/en/download/applying-the-gartner-time-framework-for-application-rationalization?utm_term=gartner%20time%20model&utm_source=bing&utm_medium=ppc&utm_campaign=EMEA-UKI_UK%20%7C%20EAM%20%7C%20Application%20Rationalization%20%7C%20Search%20%7C%20ENG%20%7C%20B&hsa_acc=9751618594&hsa_cam=590074931&hsa_grp=1145692762920984&hsa_kw=gartner%20time%20model&hsa_mt=e&hsa_net=bing&hsa_src=o&hsa_tgt=kwd-71606467990925:loc-188&hsa_ad=&hsa_ver=3&msclkid=05a8e0d21ece104a9166856dd0cc3156&utm_content=UK%20%7C%20EAM%20%7C%20gartner%20time%20framework
        ["Technical Fit", "Float", 0], // how well does the asset fit in technicall does it mesh with other systems
        ["Functional Fit", "Float", 0], // how well does it fit in with the processes it is apart of
        ["Internal Support", "Float", 0], // How much internal support is there in the business? <- does the business have answers
        ["External Support", "Float", 0], // How much external support is there in the world? <- can answers be found
        ["Cost.Low.Install", "Float", 0], // How much would it cost to install it - three point for a triangle distribution
        ["Cost.Med.Install", "Float", 0], // See above to gain a range
        ["Cost.High.Install", "Float", 0], // See above to gain a range
        ["Cost.Low.Migration", "Float", 0], // How much money would it cost to migrate to/from something else - three point for a triangle distribution
        ["Cost.Med.Migration", "Float", 0], // See above to gain a range
        ["Cost.High.Migration", "Float", 0], // See above to gain a range
        ["Cost.Low.Running", "Float", 0], // How much money does it cost to Run - three point for a triangle distribution
        ["Cost.Med.Running", "Float", 0], // See above to gain a range
        ["Cost.High.Running", "Float", 0], // See above to gain a range
    ]
}